export interface User {
  email: string;
  gender: string;
  name: {
    first: string,
    last: string,
    title: string
  };
  dob: {
    age: string,
    date: string
  };
  location: {
    city: string,
    postcode: number,
    state: string,
    street: string
  };
  nat: string;
  phone: string;
  picture: {
    large: string,
    medium: string,
    thumbnail: string
  };
}


export interface Users {
  results: User[];
}
