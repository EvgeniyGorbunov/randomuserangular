import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AboutComponent } from './pages/about/about.component';
import { UserlistComponent } from './pages/userlist/userlist.component';
import { HeaderComponent } from './components/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { RoutingModule } from './routing.module';
import { LoginComponent } from './pages/login/login.component';
import { MatFormFieldModule, MatFormFieldControl } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';
import { UserComponent } from './pages/userlist/user/user.component';
import { FormControl, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthService, DataService, ShowInfoService, LoginGurd } from './services';
import { MatMenuModule } from '@angular/material/menu';
import { NotFoundComponent } from './pages/not-found/notFound.component';




@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    UserlistComponent,
    HeaderComponent,
    LoginComponent,
    UserComponent,
    NotFoundComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatTabsModule,
    MatCardModule,
    RoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule



  ],
  providers: [
    DataService,
    ShowInfoService,
    AuthService,
    LoginGurd

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
