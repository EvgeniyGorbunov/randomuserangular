import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']

})

export class LoginComponent implements OnInit {
  public name: FormControl;
  public password: FormControl;
  public userForm: FormGroup;

  constructor(private authservice: AuthService) { }

  ngOnInit(): void {
    this.createNewForm();

  }

  createNewForm(): void {
    this.name = new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(10)]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(4)]);
    this.userForm = new FormGroup({
      name: this.name,
      password: this.password
    });

  }

  getErrorMessage() {
    return this.name.hasError('required') ? '' : 'мин длина 3 и макс 10';
  }

  auth(): void {
    if (this.userForm.valid) {
      this.authservice.login(this.name.value, this.password.value);
    }
  }
}
