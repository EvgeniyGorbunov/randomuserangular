import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ShowInfoService } from 'src/app/services/showInfo.service';
import { User } from 'src/app/model/users.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  public adress: boolean;
  public user: User;
  constructor(private showInfoService: ShowInfoService, private router: Router) { }

  ngOnInit() {
    this.user = this.showInfoService.getoneUser;
    if (!this.user) {
      this.router.navigate(['userList']);
    }
  }

  showAdreess(): void {
    this.adress = !this.adress;
  }

  close(): void {
    this.router.navigate(['userList']);
  }

}
