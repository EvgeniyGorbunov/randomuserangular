import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-notFound',
  templateUrl: './notFound.component.html',
  styleUrls: ['./notFound.component.scss']
})
export class NotFoundComponent {
  constructor() {

  }
}
