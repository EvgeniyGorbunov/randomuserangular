import { Component, Input, EventEmitter, Output } from '@angular/core';
import { User } from 'src/app/model/users.model';
import { DataService } from 'src/app/services/data.service';
import { ShowInfoService } from 'src/app/services/showInfo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent {



  @Input() user: User;
  constructor(private showInfoService: ShowInfoService, private router: Router) { }

  showInfo() {
    this.showInfoService.setUser(this.user);
    this.router.navigate(['about']);
  }

}
