import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()

export class LoginGurd implements CanActivate {


  constructor(private authService: AuthService, private router: Router) { }
  canActivate() {
    const isLogin = this.authService.isLogin;
    if (isLogin) {
      console.log(isLogin);
      this.router.navigate(['userList']);
      return false;
    } else {
      return true;
    }

  }
}
