import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Users, User } from '../model/users.model';



@Injectable()

export class DataService {



  constructor(private http: HttpClient) { }

  getUsers(): Observable<Users> {

    return this.http.get<Users>('https://randomuser.me/api/?results=10');
    // return this.http.get('https://api.persik.by/v2/content/channels');
  }

}
