import { Injectable } from '@angular/core';
import { User } from '../model/users.model';

@Injectable()

export class ShowInfoService {
  public user: User;

  constructor() {

  }

  public get getoneUser() {
    return this.user;
  }
  public setUser(item: User) {
    this.user = item;
  }
}
