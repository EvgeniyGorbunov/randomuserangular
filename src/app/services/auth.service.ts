import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()


export class AuthService {
  public erorr: any;

  constructor(private router: Router) { }

  login(name: string, password: string) {
    if (name.toLocaleLowerCase() === 'john' && password.toLocaleLowerCase() === 'smith') {
      localStorage.setItem('isLogin', 'true');
      localStorage.setItem('name', name);
      this.router.navigate(['userList']);
    } else {
      alert('Неправильный логин или пароль');
    }
  }

  logout() {
    localStorage.removeItem('isLogin');
    localStorage.removeItem('name');
    this.router.navigate(['userList']);
  }

  get loginName() {
    if (localStorage.getItem) {
      return localStorage.getItem('name');
    } else {
      return false;
    }
  }

  get isLogin() {
    if (localStorage.getItem('isLogin')) {
      return true;
    } else {
      return false;
    }
  }
}
