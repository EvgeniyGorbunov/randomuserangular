import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }
  public get isLogin() {
    return this.authService.isLogin;
  }

  public get loginName() {
    return this.authService.loginName;
  }

  public logout() {
    if (this.isLogin) {
      this.authService.logout();
    }
  }

}
