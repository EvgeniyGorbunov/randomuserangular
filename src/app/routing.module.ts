import { NgModule } from '@angular/core';
import { RouterModule, Router, Route } from '@angular/router';
import { UserlistComponent } from './pages/userlist/userlist.component';
import { AboutComponent } from './pages/about/about.component';
import { LoginComponent } from './pages/login/login.component';
import { LoginGurd } from './services';
import { NotFoundComponent } from './pages/not-found/notFound.component';

const routes: Route[] = [{
  path: '',
  redirectTo: 'userList',
  pathMatch: 'full'
}, {
  path: 'userList',
  component: UserlistComponent
}, {
  path: 'about',
  component: AboutComponent
}, {
  path: 'login',
  component: LoginComponent,
  canActivate: [LoginGurd]
},
{
  path: 'not-found',
  component: NotFoundComponent
},
{
  path: '**',
  redirectTo: 'not-found'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})

export class RoutingModule {

  constructor() { }
}
